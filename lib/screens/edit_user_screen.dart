import 'package:flutter/material.dart';

import '/models/user.dart';
import '/utils/api.dart';
import '/utils/functions.dart';

class EditUserScreen extends StatefulWidget {
    final User user;

    EditUserScreen({required this.user});
  @override
  _EditUserScreenState createState() => _EditUserScreenState();
}

class _EditUserScreenState extends State<EditUserScreen> {
    final _formKey = GlobalKey<FormState>();
    bool isCreating = false;
     
  @override
  Widget build(BuildContext context) {
      final _tffNameController = TextEditingController()..text = widget.user.name.toString();
      final _tffEmailController = TextEditingController()..text = widget.user.email.toString();
      final _tffPhoneController = TextEditingController()..text = widget.user.phone.toString();
      final _tffWebsiteController = TextEditingController()..text = widget.user.website.toString();

      InputDecoration customDecoration(labeltext){
          return InputDecoration(
              labelText: labeltext,
              fillColor: Colors.white,
              border: OutlineInputBorder(
                  borderRadius:BorderRadius.circular(25.0),
              ),
          );
      }

      Widget txtName = Container(
          margin: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
          child:TextFormField(
            decoration: customDecoration("Name"),
            keyboardType: TextInputType.name,
            controller: _tffNameController,
            textInputAction: TextInputAction.next,
            validator: (value) {
                   return (value != null && value.isNotEmpty) ? null : 'Name is required.';
                }
            )
        );

      Widget txtEmail = Container(
           margin: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
            child: TextFormField(
            decoration: customDecoration("Email"),
            keyboardType: TextInputType.emailAddress,
            controller: _tffEmailController,
            textInputAction: TextInputAction.next,
            validator: (value) {
                return (value != null && value.isNotEmpty && value.contains('@')) ? null : 'Invalid email.';
                }
            )
        );

      Widget txtPhone = Container(
           margin: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
           child: TextFormField(
           decoration: customDecoration("Phone Number"),
           keyboardType: TextInputType.phone,
           controller: _tffPhoneController,
           textInputAction: TextInputAction.next,
           validator: (value) {
               return (value != null && value.isNotEmpty) ? null : 'Phone is required.';
               }
            )
        );

       Widget txtWebsite = Container(
            margin: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
            child: TextFormField(
            decoration: customDecoration("Website Url"),
            keyboardType: TextInputType.url,
            controller: _tffWebsiteController,
            textInputAction: TextInputAction.next,
            validator: (value) {
                return (value != null && value.isNotEmpty) ? null : 'Website is required.';
                }
            )
        );


       Widget btnSubmit = Container(
           width: double.infinity,
           margin: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
           child: ElevatedButton(
               style: ElevatedButton.styleFrom(
                   shape: RoundedRectangleBorder(
                       borderRadius: new BorderRadius.circular(30.0),
                       ),
                   padding: EdgeInsets.all(20),
                   primary: Colors.blue, 
                   onPrimary: Colors.white, 
                   ),
                onPressed: () { 
                    if (_formKey.currentState!.validate()) {
                        setState(() =>isCreating = true);
                        User updatedUserInfo = User(
                            id: 1, 
                            name: _tffNameController.text, 
                            email: _tffEmailController.text, 
                            address: widget.user.address, 
                            phone: _tffPhoneController.text, 
                            website: _tffWebsiteController.text, 
                            company: widget.user.company);
                        try {
                            API().updateUser(userInfo: updatedUserInfo, id: updatedUserInfo.id.toString());
                            setState(() =>isCreating = false);
                            showSnackBar(context, "User Updated Successfully");
                            Navigator.pushReplacementNamed(context, '/');
                        } catch (e) {
                            showSnackBar(context, e.toString());
                        }
                    } else {
                        showSnackBar(context, 'Form validation failed. Check input and try again.');
                    }
                }, 
                child: Text('Update User')
            )
        );


       Widget formAddUser = Form(
           key: _formKey,
           child: Column(
               children: [
                   SizedBox(height: 16),
                   txtName,
                   txtEmail,
                   txtPhone,
                   txtWebsite,
                   isCreating ? CircularProgressIndicator() : btnSubmit
               ]
           )
       );

    return Scaffold(
      appBar: AppBar(title: Text('Edit User'),),
      body: SingleChildScrollView(child: formAddUser)
    );
  }
}