import 'package:flutter/material.dart';

import '/models/user.dart';
import '/utils/api.dart';
import '/utils/functions.dart';

class AddUserScreen extends StatefulWidget {
  @override
  _AddUserScreenState createState() => _AddUserScreenState();
}

class _AddUserScreenState extends State<AddUserScreen> {
    final _formKey = GlobalKey<FormState>();
    bool isCreating = false;

  @override
  Widget build(BuildContext context) {
      final _tffNameController = TextEditingController();
      final _tffEmailController = TextEditingController();
      final _tffPhoneController = TextEditingController();
      final _tffWebsiteController = TextEditingController();

      InputDecoration customDecoration(labeltext){
          return InputDecoration(
              labelText: labeltext,
              fillColor: Colors.white,
              border: OutlineInputBorder(
                  borderRadius:BorderRadius.circular(25.0),
              ),
          );
      }

      Widget txtName = Container(
          margin: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
          child:TextFormField(
            decoration: customDecoration("Name"),
            keyboardType: TextInputType.name,
            controller: _tffNameController,
            textInputAction: TextInputAction.next,
            validator: (value) {
                return (value != null && value.isNotEmpty) ? null : 'Name is required.';
                }
            )
        );

      Widget txtEmail = Container(
           margin: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
            child: TextFormField(
            decoration: customDecoration("Email"),
            keyboardType: TextInputType.emailAddress,
            controller: _tffEmailController,
            textInputAction: TextInputAction.next,
            validator: (value) {
                return (value != null && value.isNotEmpty && value.contains('@')) ? null : 'Invalid email.';
                }
            )
        );

        Widget txtPhone = Container(
             margin: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
            child: TextFormField(
            decoration: customDecoration("Phone Number"),
            keyboardType: TextInputType.phone,
            controller: _tffPhoneController,
            textInputAction: TextInputAction.next,
            validator: (value) {
                return (value != null && value.isNotEmpty) ? null : 'Phone is required.';
                }
            )
        );

        Widget txtWebsite = Container(
             margin: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
            child: TextFormField(
            decoration: customDecoration("Website Url"),
            keyboardType: TextInputType.url,
            controller: _tffWebsiteController,
            textInputAction: TextInputAction.next,
            validator: (value) {
                return (value != null && value.isNotEmpty) ? null : 'Website is required.';
                }
            )
        );


        Widget btnSubmit = Container(
            width: double.infinity,
            margin: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
            child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30.0),
                    ),
                    padding: EdgeInsets.all(20),
                    primary: Colors.blue, 
                    onPrimary: Colors.white,
                ),
                onPressed: () { 
                    if (_formKey.currentState!.validate()) {
                        setState(() =>isCreating = true);
                        User userInfo = User(
                            id: 1, 
                            name: _tffNameController.text, 
                            email: _tffEmailController.text, 
                            address: {"Manila":"Philippines"}, 
                            phone: _tffPhoneController.text, 
                            website: _tffWebsiteController.text, 
                            company: {"FFUF": "Manila"});

                        try {
                            API().createUser(userInfo: userInfo);
                            setState(() =>isCreating = false);
                            showSnackBar(context, "User Created Successfully");
                            Navigator.pushReplacementNamed(context, '/');
                        } catch (e) {
                            showSnackBar(context, e.toString());
                        }
                    } else {
                        showSnackBar(context, 'Form validation failed. Check input and try again.');
                    }
                }, 
                child: Text('Create User')
            )
        );


        Widget formAddUser = Form(
            key: _formKey,
            child: Column(
                children: [
                    SizedBox(height: 16),
                    txtName,
                    txtEmail,
                    txtPhone,
                    txtWebsite,
                    isCreating ? CircularProgressIndicator() : btnSubmit
                ]
            )
        );

    return Scaffold(
      appBar: AppBar(title: Text('Add User'),),
      body: SingleChildScrollView(child: formAddUser)
    );
  }
}