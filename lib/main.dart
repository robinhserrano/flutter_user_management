import 'package:flutter/material.dart';
import 'package:flutter_user_management/screens/add_user_screen.dart';
import '/screens/user_list_screen.dart';

void main() {
  runApp(App());
}

class App extends StatelessWidget {
   @override
   Widget build(BuildContext context) {
       return MaterialApp(             
           initialRoute: '/',
           routes: {
               '/': (context) => UserListScreen(),
               '/add-user-screen': (context) => AddUserScreen(),
            } 
       ); 
    }  
}
