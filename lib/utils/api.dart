import 'package:dio/dio.dart';

import '/models/user.dart';

class API {
    final Dio _dio = Dio();
    final _baseUrl = 'https://jsonplaceholder.typicode.com';

	Future getUsers() async {
		try {
			String url = 'https://jsonplaceholder.typicode.com/users';
			var response = await Dio().get(url);
            List<User> users = [];

            for (var user in response.data) {
              users.add(User.fromJson(user));
            }
            return users;
		} catch (exception) {
			throw exception;
		}
	}


    Future<User?> createUser({required User userInfo}) async {

        User? retrievedUser;

        try{
            Response response = await _dio.post(
                _baseUrl + '/users',
                data: userInfo.toJson()
            );

            print('User created: ${response.data}');
            retrievedUser = User.fromJson(response.data);
            return retrievedUser;

        } catch (e) {
            print('Error creating user: $e');
        }
    }
         
    Future<User?> updateUser({
        required User userInfo,
        required String id,
        }) async {

        User? updatedUser;

        try {
            Response response = await _dio.put(
                _baseUrl + '/users/$id',
                data: userInfo.toJson(),
            );

            print('User updated: ${response.data}');
            updatedUser = User.fromJson(response.data);
            return updatedUser;

        } catch (e) {
            print('Error updating user: $e');
        }
    }
}