import 'package:flutter/material.dart';

class AppDrawer extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        return Drawer(
            child: Container(
                width: double.infinity,
                color: Color.fromRGBO(20, 45, 68, 1),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                        DrawerHeader(
                               child: Column(
                               crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                      SizedBox(height: 36.0),
                                      Container(
                                          margin: EdgeInsets.only(top: 16.0),
                                          child: Column(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: [
                                                  Text('User Management', style: TextStyle(fontSize: 20.0, color: Colors.white)),
                                                  ListTile(
                                                      title: Text('Add User', style: TextStyle(color: Colors.white)),
                                                      leading: Icon(Icons.account_circle, color: Colors.white,),
                                                      onTap: () async {
                                                      Navigator.pushNamed(context, '/add-user-screen');
                                                      }
                                                  ),
                                              ]
                                          )
                                      )
                                  ]
                             ),
                        ),
                        Spacer(),
                    ]
                )
            )
        );
    }
}